# Example GraphQL API

## Technologies
- docker
- mongo
- mongo-express
- graphql


## Dev
```bash
# install docker compass
docker-compose up
```

## Mongo
Create a mongo instance with 3 collections:
* b2c
* b2b_soho
* b2b_small

Upload the csv's through mongo compass.

## Queries

### getProduct

#### Query
```graphql
query getProduct($product: InputProduct!, $set: DataSet!) {
  product(product: $product, set: $set) {
    Name,
    OfferName,
    OneOffCosts
  }
}
```

#### Vars
```graphql
{
  "product": {
    "Name": "TV Test"
  },
  "set": "b2c"
}
```

### getProducts

#### Query
```graphql
query getAllProducts($set: DataSet!) {
  products(set: $set) {
    Name,
  }
}

```

#### Vars
```graphql
{
  "set": "b2c"
}
```


## Mutations

### addProduct

#### Query
```graphql
mutation addProduct($product: InputProduct!, $set: DataSet!) {
  addProduct(product: $product, set: $set) {
    Name
  }
}
```

#### Vars
```graphql
{
  	"set": "b2c",
    "product": {
      "Name": "TV Test",
      "OfferName": "Second Test",
      "ProductFamily": "bla",
      "ProductType": "test",
      "IsVisible": "TRUE"
    }
}
```

### deleteProduct

#### Query
```graphql
mutation deleteProduct($product: InputProduct!, $set: DataSet!) {
  deleteProduct(product: $product, set: $set) {
    Error,
    Name
  }
}
```

#### Vars
```graphql
{
  "product": {
    "Name": "TV Test"
  },
  "set": "b2c"
}
```

### updateProduct

#### Query
```graphql
mutation updateProduct($product: InputProduct!, $update: InputProduct!, $set: DataSet!) {
  updateProduct(product: $product, update: $update, set: $set) {
    Name
  }
}

```

#### Vars
```graphql
{
  "set": "b2c",
  "update": {
    "Name": "TV Test",
    "OfferName": "Second Test - updatet",
    "ProductFamily": "bla",
    "ProductType": "test",
    "IsVisible": "TRUE"
  },
  "product": {
    "Name": "TV Test"
  }
}
```