ARG access_token

FROM node:12.3.1-alpine as build

ARG access_token

ENV ACCESS_TOKEN=$access_token

# docker workdir
WORKDIR /usr/src/app

# install dependencies for node-gyp
RUN apk add --update --no-cache \
    python \
    make \
    g++

COPY . .

RUN npm ci && npm run build

EXPOSE 8080

CMD ["npx", "pm2-runtime", "lib/index.js"]
