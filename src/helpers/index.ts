import winston from 'winston';

// utility method used
// to store a log file 
// on the server
const logger = ( () => {
  const transports = [
    new winston.transports.File({
      dirname: 'logs',
      filename: 'error.log',
      level: 'error',
    }),
    new winston.transports.File({
      dirname: 'logs',
      filename: 'combined.log',
      level: 'verbose',
    }),
  ];
  
  if (process.env.NODE_ENV !== 'production') {
    transports.push(new winston.transports.Console() as any);
  }

  return winston.createLogger({
    format: winston.format.json(),
    level: 'info',
    transports,
  })
})();

export default {
  logger,
}
