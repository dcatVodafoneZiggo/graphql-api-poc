import * as mutation from './mutation'
import * as query from './query'
import * as models from '../../../models'

export default {
  Query: {
    async product(_root: any, args: models.IGetProduct, _context: any) {
      return await query.getProduct(args)
    },
    async products(_root: any, args: models.IGetProducts) {
      return await query.getProducts(args)
    },
  },
  Mutation: {
    async addProduct(_root: any, obj: models.IAddProduct, _context: any) {
      return await mutation.addProduct(obj);
    }, 
    async deleteProduct(_root: any, obj: models.IDeleteProduct, _context: any) {
      return await mutation.deleteProduct(obj)
    },
    async updateProduct(_root: any, obj: models.IUpdateProduct, _context: any) {
      return await mutation.updateProduct(obj)
    },
  },
};