import * as models from '../../../../models'
import * as db from '../../../../server/mongo'

export default async ({ product, update, set }: models.IUpdateProduct) => {
    return await db.update(product, update, set);
}