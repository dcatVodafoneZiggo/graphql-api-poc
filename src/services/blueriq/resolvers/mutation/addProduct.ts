import * as models from '../../../../models'
import * as db from '../../../../server/mongo'

export default async ({ product, set }: models.IAddProduct) => {
    return await db.insert({
        ...product
    }, set);
}