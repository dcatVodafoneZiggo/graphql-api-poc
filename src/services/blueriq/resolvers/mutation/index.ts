export { default as addProduct } from './AddProduct'
export { default as deleteProduct } from './DeleteProduct'
export { default as updateProduct } from './UpdateProduct'