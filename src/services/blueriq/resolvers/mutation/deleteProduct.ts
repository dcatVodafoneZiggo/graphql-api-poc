import * as models from '../../../../models'
import * as db from '../../../../server/mongo'

export default async ({ product, set }: models.IDeleteProduct) => {
    return await db.delete(product, set);
}