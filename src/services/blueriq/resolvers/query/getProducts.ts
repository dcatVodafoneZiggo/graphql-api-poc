import * as db from '../../../../server/mongo';
import * as models from '../../../../models'

export default async ({ set }: models.IGetProducts) => {
    return await db.query({}, set)
}