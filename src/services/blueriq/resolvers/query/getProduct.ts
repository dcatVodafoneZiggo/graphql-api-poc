import * as models from '../../../../models'
import * as db from '../../../../server/mongo'

export default async ({ product, set }: models.IGetProduct) => {
    return await db.query({
      ...product
    }, set)
}