import { gql } from 'apollo-server-express'

const root = gql`
    type Mutation {
        root: String
    }

    type Query {
        root: String
    }
`

export default root