import * as mutationSchemas from './mutation'
import * as querySchemas from './query'
import root from './root'
import shared from './shared'

const schemaArr: any[] = [root, shared];

for ( const i in mutationSchemas ) {
    schemaArr.push((mutationSchemas as any)[i])
}

for ( const i in querySchemas ) {
    schemaArr.push((querySchemas as any)[i])
}

export default schemaArr