import { gql } from 'apollo-server-express'

const getProduct = gql`
    extend type Query {
        product(product: InputProduct!, set: DataSet!): [Product!]
    }
`

export default getProduct