import { gql } from 'apollo-server-express'

const getProduct = gql`
    extend type Query {
        products(set: DataSet!): [Product]!
    }
`

export default getProduct