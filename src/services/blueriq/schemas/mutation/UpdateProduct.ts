import { gql } from 'apollo-server-express'

const updateProduct = gql`
    extend type Mutation {
        updateProduct(product: InputProduct!, update: InputProduct!, set: DataSet!): Product!
    }
`

export default updateProduct