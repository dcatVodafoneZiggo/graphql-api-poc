import { gql } from 'apollo-server-express'

const addProduct = gql`
    extend type Mutation {
        addProduct(product: InputProduct!, set: DataSet!): Product!
    }
`

export default addProduct