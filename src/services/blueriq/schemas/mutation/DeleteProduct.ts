import { gql } from 'apollo-server-express'

const deleteProduct = gql`
    extend type Mutation {
        deleteProduct(product: InputProduct!, set: DataSet!): Product!
    }
`

export default deleteProduct