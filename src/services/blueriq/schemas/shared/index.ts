import { gql } from 'apollo-server-express'

const shared = gql`
    enum DataSet {
        b2c,
        b2b_small,
        b2b_soho
    }

    type Product {
        OfferName: String
        OfferGroupId: String
        Name: String
        ProductFamily: String
        ProductType: String
        SubType: String
        Group: String
        IncludedItems: String
        IsVisible: String
        OneOffCosts: String
        MonthlyPrice: String
        SetupPrice: String
        BindingPeriod: String
        ApplicableDiscount: String
        PromoValue: String
        PromoPrice: String
        PromoType: String
        DurationPromo: String
        PromoSegment: String
        Error: Boolean
        lastUpdate: String
        Count: Int
    }

    input InputProduct {
        OfferName: String
        OfferGroupId: String
        Name: String
        ProductFamily: String
        ProductType: String
        SubType: String
        Group: String
        IncludedItems: String
        IsVisible: String
        OneOffCosts: String
        MonthlyPrice: String
        SetupPrice: String
        BindingPeriod: String
        ApplicableDiscount: String
        PromoValue: String
        PromoPrice: String
        PromoType: String
        DurationPromo: String
        PromoSegment: String
        lastUpdate: String
    }

`

export default shared