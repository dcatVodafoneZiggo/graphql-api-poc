import { ApolloServer } from 'apollo-server-express';
import resolvers from './resolvers'
import typeDefs from './schemas'

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ res, req }) => ({ res, req}),
})

export default server