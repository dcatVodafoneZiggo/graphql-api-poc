import { ChangeStream, MongoClient } from 'mongodb'

function closeChangeStream(changeStream: ChangeStream, timeInMs = 60000,) {
    return new Promise((resolve) => {
        setTimeout(() => {
            console.log("Closing the change stream");
            changeStream.close();
            resolve();
        }, timeInMs)
    })
};

async function monitorListingsUsingEventEmitter(client: MongoClient, pipeline: any[] = []){
    const { DB_NAME, DB_COLLECTION } = process.env;

    const changeStream = client.db(DB_NAME).collection(DB_COLLECTION).watch(pipeline)

    changeStream.on('change', next => {
        console.log(next)
    })

    await closeChangeStream(changeStream)

}

export default {
    monitorListingsUsingEventEmitter
}