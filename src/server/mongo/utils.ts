import { MongoClient, Db } from 'mongodb';
import utils from '../../helpers'
import stream from '../streams'

function createMongo() {
  const URI = process.env.MONGO_URL;

  // return a connection with mongo
  return new MongoClient(URI, {
    useUnifiedTopology: true,
  });
}

function connectToMongo(): Promise<Db> {
  return new Promise( (resolve, reject) => {
    
    const { DB_NAME } = process.env

    // mongo has already
    if (createMongo().isConnected()) {
      resolve(
        createMongo()
        .db(DB_NAME)
      )
    }

    createMongo().connect( async (err, client) => {

      if (err) {
        // log our error
        // in a error.log
        utils.logger.log({
          level: 'error',
          message: err.message,
        });
    
        // reject the promise
        // and throw an error
        reject(err);
      }

      // set the listener to close
      // the connection
      closeMongoListener(client)

      if (process.env.NODE_ENV !== 'development') {
        await stream.monitorListingsUsingEventEmitter(client)
      }

      resolve(
        client
        .db(DB_NAME)
      )
    })
  })  
}

function closeMongoListener(client:MongoClient) {
  process.on("SIGTERM", async () => {
    await client.close()
  })
}

export default {
  connectToMongo,
};
