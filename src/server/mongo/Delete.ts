import helpers from '../../helpers';
import * as models from '../../models'
import db from './utils';

interface IIDeleteResp extends Partial<models.IProduct> {
  Error: boolean
}

function deleteProduct(product: Partial<models.IProduct>, set: keyof typeof models.DataSets): Promise<IIDeleteResp> {
  return new Promise( async (resolve, reject) => {
    const collection = (await db.connectToMongo()).collection(set);

    collection.remove(
      { 
        ...product,
      },
      (err, resp) => {
        if (err) {
          helpers.logger.log({
            level: 'error',
            message: err.message
          });

          reject(err)
        }
  
        resolve({
          Error: resp.result.ok === 0,
          ...product,
        });
      },
    )
  })
  
}

export default deleteProduct;
