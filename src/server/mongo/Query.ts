// import logger from '../../helpers/logger';
import * as models from '../../models'
import db from './utils';

async function query<T>(qry: T, set: keyof typeof models.DataSets) {
  const collection = (await db.connectToMongo()).collection(set);

  return await collection.find<models.IProduct>(qry).toArray();
}

export default query;
