// import logger from '../../helpers/logger';
import db from './utils';
import * as models from '../../models'

interface IInsertResp extends Partial<models.IProduct>{
  Error: boolean
  Count: number
}

async function insert(product: Partial<models.IProduct>, set: keyof typeof models.DataSets): Promise<IInsertResp> {
  const collection = (await db.connectToMongo()).collection(set);

  const resp = await collection.insertOne({
    ...product,
    lastUpdate: new Date().toISOString()
  });

  return {
    Count: resp.insertedCount,
    Error: resp.result.ok === 0,
    ...product,
  }
}

export default insert;
