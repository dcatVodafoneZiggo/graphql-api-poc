import helpers from '../../helpers';
import * as models from '../../models'
import db from './utils';

interface IUpdateResp extends Partial<models.IProduct> {
  Error: boolean,
  Count: number,
}

function update(product: Partial<models.IProduct>, update: Partial<models.IProduct>, set: keyof typeof models.DataSets): Promise<IUpdateResp> {
  return new Promise( async (resolve, reject) => {
    const collection = (await db.connectToMongo()).collection(set);

    collection.updateMany(
      { 
        ...product
      },
      {
        $set: { 
          ...update,
          lastUpdate: new Date().toISOString()
         },
      },
      (err, resp) => {
  
        if (err) {
          helpers.logger.log({
            level: 'error',
            message: err.message
          })

          reject(err)
        }
  
        resolve({
          Error: resp.result.ok === 0,
          Count: resp.modifiedCount,
          ...product,
          ...update
        });
      },
    )
  })
  
}

export default update;
