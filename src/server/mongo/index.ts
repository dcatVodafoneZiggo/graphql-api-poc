export { default as update } from './Update';
export { default as insert } from './Insert';
export { default as query } from './Query';
export { default as delete } from './Delete';