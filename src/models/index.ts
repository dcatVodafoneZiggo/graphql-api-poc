export enum DataSets {
    b2b_small = "b2b_small",
    b2b_soho = "b2b_soho",
    b2c = "b2c",
}

export interface IProduct {
    OfferName: string
    OfferGroupId: string
    Name: string
    ProductFamily: string
    ProductType: string
    SubType: string
    Group: string
    IncludedItems: string
    IsVisible: string
    OneOffCosts: string
    MonthlyPrice: string
    SetupPrice: string
    BindingPeriod: string
    ApplicableDiscount: string
    PromoValue: string
    PromoPrice: string
    PromoType: string
    DurationPromo: string
    PromoSegment: string
    Error: boolean
    lastUpdate: string
    Count: number
}

export interface IUpdateProduct {
    product: Partial<IProduct>,
    update: Partial<IProduct>,
    set: keyof typeof DataSets
}

export interface IAddProduct {
    product: Partial<IProduct>,
    set: keyof typeof DataSets
}

export interface IDeleteProduct {
    product: Partial<IProduct>,
    set: keyof typeof DataSets
}

export interface IGetProduct {
    product: Partial<IProduct>,
    set: keyof typeof DataSets
}

export interface IGetProducts {
    set: keyof typeof DataSets
}