import 'dotenv/config';

import compress from 'compression';
import cors from 'cors';
import express from 'express';
import http from 'http';

// import all services
// for each service we reserve a different endpoint
import services from './src/services';

const PORT = 8080;

// create a new express app
const app = express();

// use compression to save bandwidth
app.use(compress());

// use cross origin
app.use(cors());

const server = http.createServer(app);

// if we go to the homepage
// return the following
app.get('/', (_req, res) => {
  // return the page
  res.send({
    "success": true
  });
});

server.listen(PORT);

const serviceNames = Object.keys(services);

// we splitted the API's into blueriq
// and into AEM

// blueriq is for mutating
// AEM is used for querying

for (const i in serviceNames) {
  if (serviceNames[i] === 'blueriq') {
    // @ts-ignore
    services[serviceNames[i]].applyMiddleware({ app });
  } 
}
